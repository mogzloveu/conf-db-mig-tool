# Overview
This tool will help you to export and import Confluence database directly from the database. It is similar to performing database dump, however creating the dump does not need DBA level database permission. Importing the data into the database needs DBA level permission. The tool involves GUI which needs you to run the tool on a machine that has GUI (you can't run this tool on a headless server).

# Pre-requisites
1. The Confluence version needs to be the same.
2. The installed plugins needs to be installed on the destination/target Confluence instance.

# Script Configuration & Installation
1. [Download the script](https://bitbucket.org/kennyngkk/database-migration-tool/get/aa7c82801c2a.zip)
2. If you are using Ubuntu/MAC, 
	1. We need to install some dependencies, run the following command:  
	`cd <script directory>`  
	`sudo pip install -r requirement.txt`
	2. You may create an alias to execute the script anywhere in the OS.
	
3. If you are using Windows, please execute the `db_mig.exe` file.
4. For MySQL database, you will need to install MySQL Connector/Python from [Download Connector/Python](https://dev.mysql.com/downloads/connector/python/).

# Usage
To migrate data from one Confluence instance to a new Confluence instance:

1. Install a new Confluence as per the usual installation steps and connect it to an empty data.
2. Make sure the Confluence version is the same and also you have installed all the plugins that you are using on the source instance.
3. Execute the script:
	* On Linux:  
	`cd <script directory>`  
	`python db_mig.py`
	* On Windows, double click on the `db_mig.exe` file.
	
4. Provide all the required fields for the export and import.

Please test the tool on a test database to make sure you are familiar with the steps before exporting and importing your Production database.

# Feedback, Bugs and New Features
Please raise any feedback, bugs and new features in the [issue tracker](https://bitbucket.org/kennyngkk/database-migration-tool/issues).