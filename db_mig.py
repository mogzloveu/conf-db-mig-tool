# -*- coding: utf-8 -*-
# description     :Python script to manually create Confluence database backup and restore Confluence database.
# author          :This is a ShipIT 41 project (SHPXLI-72). Credits goes to kng, mmuthusamy and kkujaafar.

try:
    import Tkinter as tk # this is for python2
except:
    import tkinter as tk # this is for python3
try:
    import tkFileDialog as tkFileDialog # this is for python2
except:
    from tkinter import filedialog as tkFileDialog # this is for python3
import os
import database
import sys

LARGE_FONT= ("Verdana", 12)

class guiStart(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        tk.Tk.iconbitmap(self,default=self.resource_path(os.path.join('data', 'favicon.ico')))
        tk.Tk.wm_title(self, "Confluence Database Migration Tool")
        container = tk.Frame(self)
        container.pack(side="top", fill="both", expand = True)
        container.grid_rowconfigure(0, weight=1)
        container.grid_columnconfigure(0, weight=1)

        self.frames = {}
        for F in (startPage, importPage, exportPage):
            frame = F(container, self)
            self.frames[F] = frame
            frame.grid(row=0, column=0, sticky="nsew")
        self.show_frame(startPage)

    def show_frame(self, cont):
        frame = self.frames[cont]
        frame.tkraise()

    # Workaround to include favicon.ico from https://irwinkwan.com/2013/04/29/python-executables-pyinstaller-and-a-48-hour-game-design-compo/
    def resource_path(self, relative):
        if hasattr(sys, "_MEIPASS"):
            return os.path.join(sys._MEIPASS, relative)
        return os.path.join(relative)

class startPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.startPageLabel = tk.Label(self, text="Confluence Database Migration Tool", font=LARGE_FONT)
        self.startPageLabel.pack(pady=10,padx=10)
        self.exportButton = tk.Button(self, text='Export', width=25, command=lambda: controller.show_frame(exportPage))
        self.exportButton.pack(side = 'left',pady=10,padx=10)
        self.importButton = tk.Button(self, text='Import', width=25, command=lambda: controller.show_frame(importPage))
        self.importButton.pack(side = 'left',pady=10,padx=10)

class importPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.importPageLabel = tk.Label(self, text="Import details", font=LARGE_FONT)
        self.importPageLabel.pack(pady=10,padx=10)

        self.importDatabaseTypeStr = tk.StringVar()
        self.importDatabaseTypeLabel = tk.Label(self, text="Database Type")
        self.importDatabaseTypeLabel.pack()
        self.importEntryBoxDatabaseTypeList = ["postgresql", "mysql+mysqlconnector"]
        self.importEntryBoxDatabaseTypeDrop = tk.OptionMenu(self, self.importDatabaseTypeStr, *self.importEntryBoxDatabaseTypeList)
        self.importEntryBoxDatabaseTypeDrop.pack()

        self.importDatabaseServerStr = tk.StringVar()
        self.importDatabaseServerLabel = tk.Label(self, text="Database Server")
        self.importDatabaseServerLabel.pack()
        self.importEntryBoxDatabaseServer = tk.Entry(self, textvariable=self.importDatabaseServerStr, width=25)
        self.importEntryBoxDatabaseServer.pack()

        self.importDatabasePortStr = tk.StringVar()
        self.importDatabasePortLabel = tk.Label(self, text="Database Port")
        self.importDatabasePortLabel.pack()
        self.importEntryBoxDatabasePort = tk.Entry(self, textvariable=self.importDatabasePortStr, width=25)
        self.importEntryBoxDatabasePort.pack()

        self.importDatabaseUserStr = tk.StringVar()
        self.importDatabaseUserLabel = tk.Label(self, text="Database User")
        self.importDatabaseUserLabel.pack()
        self.importEntryBoxDatabaseUser = tk.Entry(self, textvariable=self.importDatabaseUserStr, width=25)
        self.importEntryBoxDatabaseUser.pack()
        
        self.importDatabasePasswordStr = tk.StringVar()
        self.importDatabasePasswordLabel = tk.Label(self, text="Database Password")
        self.importDatabasePasswordLabel.pack()
        self.importEntryBoxDatabasePassword = tk.Entry(self, textvariable=self.importDatabasePasswordStr, width=25)
        self.importEntryBoxDatabasePassword.pack()

        self.importDatabaseNameStr = tk.StringVar()
        self.importDatabaseNameLabel = tk.Label(self, text="Database Name")
        self.importDatabaseNameLabel.pack()
        self.importEntryBoxDatabaseName = tk.Entry(self, textvariable=self.importDatabaseNameStr, width=25)
        self.importEntryBoxDatabaseName.pack()
        
        self.importFromStr = tk.StringVar()
        self.importButton = tk.Button(self, text = 'ZIP File path', width = 25, command = lambda: self.importFromFile())
        self.importButton.pack(pady=10,padx=10)
        self.importEntryBoxImportFrom = tk.Entry(self, textvariable=self.importFromStr, width=25)
        self.importEntryBoxImportFrom.pack()

        self.importButton = tk.Button(self, text = 'Start Import', width = 25, command = lambda: self.importSaveInput())
        self.importButton.pack(side="left",pady=10,padx=10)
        self.importQuitButton = tk.Button(self, text = 'Back', width = 25, command = lambda: controller.show_frame(startPage))
        self.importQuitButton.pack(side="left",pady=10,padx=10)

    def importFromFile(self):
        self.importFrom = tkFileDialog.askopenfilename(initialdir = "/",title = "Select file",filetypes = (("ZIP files","*.zip"),("all files","*.*")))
        self.importEntryBoxImportFrom.insert(0, self.importFrom)

    def importSaveInput(self):
        self.db_details = {}
        self.db_details['databaseType'] = str(self.importDatabaseTypeStr.get())
        self.db_details['databaseServer'] = str(self.importDatabaseServerStr.get())
        self.db_details['databasePort'] = str(self.importDatabasePortStr.get())
        self.db_details['databaseUser'] = str(self.importDatabaseUserStr.get())
        self.db_details['databasePassword'] = str(self.importDatabasePasswordStr.get())
        self.db_details['databaseName'] = str(self.importDatabaseNameStr.get())
        self.db_details['exportTo'] = ''
        self.db_details['importFrom'] = str(self.importFromStr.get())

        # Calling insertTable() function from database.py
        self.database = database.Database(self.db_details)
        self.database.insertTable() 

        # Pop up a new UI to show the content of import.log file
        self.popup = tk.Toplevel(self)
        tk.Label(self.popup, text="Importing data")
        self.importLog = os.path.join(os.path.dirname(self.db_details['importFrom']), "export", "import", "import.log")
        self.logFile = open(self.importLog, "r")
        self.line = self.logFile.read()
        self.tex = tk.Text(self.popup)
        self.tex.pack(side="bottom")
        self.tex.insert(tk.END, self.line)
        self.tex.see(tk.END)
        self.logFile.close()

class exportPage(tk.Frame):
    def __init__(self, parent, controller):
        tk.Frame.__init__(self, parent)
        self.exportPageLabel = tk.Label(self, text="Export details", font=LARGE_FONT)
        self.exportPageLabel.pack(pady=10,padx=10)

        self.exportDatabaseTypeStr = tk.StringVar()
        self.exportDatabaseTypeLabel = tk.Label(self, text="Database Type")
        self.exportDatabaseTypeLabel.pack()
        self.exportEntryBoxDatabaseTypeList = ["postgresql", "mysql+mysqlconnector"]
        self.exportEntryBoxDatabaseTypeDrop = tk.OptionMenu(self, self.exportDatabaseTypeStr, *self.exportEntryBoxDatabaseTypeList)
        self.exportEntryBoxDatabaseTypeDrop.pack()

        self.exportDatabaseServerStr = tk.StringVar()
        self.exportDatabaseServerLabel = tk.Label(self, text="Database Server")
        self.exportDatabaseServerLabel.pack()
        self.exportEntryBoxDatabaseServer = tk.Entry(self, textvariable=self.exportDatabaseServerStr, width=25)
        self.exportEntryBoxDatabaseServer.pack()

        self.exportDatabasePortStr = tk.StringVar()
        self.exportDatabasePortLabel = tk.Label(self, text="Database Port")
        self.exportDatabasePortLabel.pack()
        self.exportEntryBoxDatabasePort = tk.Entry(self, textvariable=self.exportDatabasePortStr, width=25)
        self.exportEntryBoxDatabasePort.pack()

        self.exportDatabaseUserStr = tk.StringVar()
        self.exportDatabaseUserLabel = tk.Label(self, text="Database User")
        self.exportDatabaseUserLabel.pack()
        self.exportEntryBoxDatabaseUser = tk.Entry(self, textvariable=self.exportDatabaseUserStr, width=25)
        self.exportEntryBoxDatabaseUser.pack()
        
        self.exportDatabasePasswordStr = tk.StringVar()
        self.exportDatabasePasswordLabel = tk.Label(self, text="Database Password")
        self.exportDatabasePasswordLabel.pack()
        self.exportEntryBoxDatabasePassword = tk.Entry(self, textvariable=self.exportDatabasePasswordStr, width=25)
        self.exportEntryBoxDatabasePassword.pack()

        self.exportDatabaseNameStr = tk.StringVar()
        self.exportDatabaseNameLabel = tk.Label(self, text="Database Name")
        self.exportDatabaseNameLabel.pack()
        self.exportEntryBoxDatabaseName = tk.Entry(self, textvariable=self.exportDatabaseNameStr, width=25)
        self.exportEntryBoxDatabaseName.pack()

        self.exportFromStr = tk.StringVar()
        self.exportFromLabel = tk.Label(self, text="Export To (path pf the ZIP file)")
        self.exportFromLabel.pack()
        self.exportEntryBoxexportFrom = tk.Entry(self, textvariable=self.exportFromStr, width=25)
        self.exportEntryBoxexportFrom.pack()

        self.exportButton = tk.Button(self, text = 'Start export', width = 25, command = lambda: self.exportSaveInput())
        self.exportButton.pack(side="left",pady=10,padx=10)
        self.exportQuitButton = tk.Button(self, text = 'Back', width = 25, command = lambda: controller.show_frame(startPage))
        self.exportQuitButton.pack(side="left",pady=10,padx=10)

    def exportSaveInput(self):
        self.db_details = {}
        self.db_details['databaseType'] = str(self.exportDatabaseTypeStr.get())
        self.db_details['databaseServer'] = str(self.exportDatabaseServerStr.get())
        self.db_details['databasePort'] = str(self.exportDatabasePortStr.get())
        self.db_details['databaseUser'] = str(self.exportDatabaseUserStr.get())
        self.db_details['databasePassword'] = str(self.exportDatabasePasswordStr.get())
        self.db_details['databaseName'] = str(self.exportDatabaseNameStr.get())
        self.db_details['exportTo'] = str(self.exportFromStr.get())
        self.db_details['importFrom'] = ''

        # Calling exportTable() function from database.py
        self.database = database.Database(self.db_details)
        self.database.exportTable()

        # Pop up a new UI to show the content of export.log file
        self.popup = tk.Toplevel(self)
        tk.Label(self.popup, text="Exporting data")
        self.exportLog = os.path.join(self.db_details['exportTo'], "export", "stats", "export.log")
        self.logFile = open(self.exportLog, "r")
        self.line = self.logFile.read()
        self.tex = tk.Text(self.popup)
        self.tex.pack(side="bottom")
        self.tex.insert(tk.END, self.line)
        self.tex.see(tk.END)
        self.logFile.close()

def main():
    app = guiStart()
    app.mainloop()

if __name__ == '__main__':
    main()