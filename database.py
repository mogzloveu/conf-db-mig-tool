# -*- coding: utf-8 -*-
# description     :Python script to manually create Confluence database backup and restore Confluence database.
# author          :This is a ShipIT 41 project (SHPXLI-72). Credits goes to kng, mmuthusamy and kkujaafar.

import sqlalchemy
import os
import datetime
from decimal import *
import zipfile

class Database(object):
    def __init__(self, database):
    # Variables initialization
        self.databaseType = database['databaseType']
        self.databaseServer = database['databaseServer']
        self.databasePort = database['databasePort']
        self.databaseUser = database['databaseUser']
        self.databasePassword = database['databasePassword']
        self.databaseName = database['databaseName']
        
        self.exportTo = database['exportTo']
        self.basePath = database['importFrom']
        
        self.dbUrl = self.databaseType + '://' + self.databaseUser + ':' + self.databasePassword + '@' + self.databaseServer + ':' + self.databasePort + '/' + self.databaseName
        
        self.exportFolder = os.path.join(self.exportTo, "export")
        self.statsFolder = os.path.join(self.exportFolder, "stats")
        self.tableDumpFolder = os.path.join(self.exportFolder, "tabledumps")
        self.exportDbTableStatsLogfile = os.path.join(self.statsFolder, "export_db_table_stats.log")
        self.exportLogfile = os.path.join(self.statsFolder, "export.log")

        self.basePathFolder = os.path.join(os.path.dirname(self.basePath), "export")
        self.importFolder = os.path.join(self.basePathFolder, "import")
        self.importLogfile = os.path.join(self.importFolder, "import.log")
        self.importDbTableStatsLogfile = os.path.join(self.importFolder, "import_db_table_stats.log")
        self.importDbDumpFiles = os.path.join(self.basePathFolder, "tabledumps")

        self.engine = sqlalchemy.create_engine(self.dbUrl)
        self.conn = self.engine.connect()
        self.meta = sqlalchemy.MetaData(self.engine,reflect=True)

    # Export table dumps
    def exportTable(self):
        os.makedirs(self.statsFolder)
        os.makedirs(self.tableDumpFolder)
        
        # Get all the tables
        self.allTables = self.engine.table_names()
        for self.table_name in self.allTables:
            self.selectStarTableFileName = os.path.join(self.tableDumpFolder, str(self.table_name) + '.txt')
            self.selectStarTableFile = open(self.selectStarTableFileName,"w+")

            # Generating export.log
            self.exportLog = open(self.exportLogfile,"a")
            self.exportLog.write('Exporting data from %s.\n' %self.table_name)
            self.exportLog.close()

            # Generating export summary log.
            if self.databaseType == 'postgresql':
                self.rowCountSql = 'SELECT COUNT(*) FROM ' + '"%s"' %str(self.table_name)
            if self.databaseType == 'mysql+mysqlconnector':
                self.rowCountSql = 'SELECT COUNT(*) FROM ' + '`%s`.' %str(self.databaseName) + '%s' %str(self.table_name)
            self.rowCount = self.engine.execute(self.rowCountSql)
            self.rowCountResult = self.rowCount.fetchall()
            self.rowCountResult = self.rowCountResult[0][0]
            self.allTablesFile= open(self.exportDbTableStatsLogfile,"a")
            self.allTablesFile.write("%s:%s\n" % (self.table_name, self.rowCountResult))
            self.allTablesFile.close()
            
            # This is "select * table_name"
            self.table =  self.meta.tables[self.table_name]
            self.selectStarTableSql = self.table.select()
            
            # Select * from table
            self.selectStarTableResult = self.conn.execute(self.selectStarTableSql)
            for self.data in self.selectStarTableResult:
                # Getting the lines from the result in dict format
                self.data = dict(self.data)
                self.selectStarTableFile.write("%s\n" %self.data)
            self.selectStarTableFile.close()

        self.exportLog = open(self.exportLogfile,"a")
        self.exportLog.write('Export is complete!\n')
        self.exportLog.close()

        self.countExportTable()
        self.countExportLines()

        # Zipping all the content to export.zip file.
        self.zipFile()
            
    # Count number of exported tables and check with the lines in export_db_table_stats.log.
    def countExportTable(self):
        #self.numberOfTableFile = len(os.walk(self.tableDumpFolder).next()[2])
        #Edited this line above to the below one to make it compatible with python3.
        self.numberOfTableFile = len(next(os.walk(self.tableDumpFolder))[2])
        self.numberOfLineTableStat = 0
        with open(self.exportDbTableStatsLogfile, 'r') as f:
            for line in f:
                self.numberOfLineTableStat += 1
        f.close

        if self.numberOfTableFile == self.numberOfLineTableStat:
            self.exportLog = open(self.exportLogfile,"a")
            self.exportLog.write('\nTable export count is correct.\n')
            self.exportLog.close()
        else:
            self.exportLog = open(self.exportLogfile,"a")
            self.exportLog.write('\nTable export count is not correct!\n')
            self.exportLog.close()

    # Count number of lines in each exported table dump file and check with the row count in export_db_table_stats.log.
    def countExportLines(self):
        # Read the export_db_table_stats.log and store in as a dictonary.
        dict = {}
        with open(self.exportDbTableStatsLogfile, 'r') as n:
            for line in n:
                (key, val) = line.strip().split(':')
                dict[key] = val
        n.close

        #Open each file and get the line count and then check with the dict.
        for filename in os.listdir(self.tableDumpFolder):
            tableName = filename.split(".")[0]

            self.numberOfFileLine = 0
            with open(os.path.join(self.tableDumpFolder, filename), 'r') as f:
                for line in f:
                    self.numberOfFileLine += 1
            f.close
            
            if (int(dict[tableName]) == self.numberOfFileLine):
                pass
            else:
                self.exportLog = open(self.exportLogfile,"a")
                self.exportLog.write('\nNumber of exported lines for %s does not match!\n' %tableName)
                self.exportLog.close()

    # ZIP the export contents.
    def zipFile(self):
        zfile = zipfile.ZipFile(os.path.join(self.exportFolder, 'export.zip'), 'w', zipfile.ZIP_DEFLATED, allowZip64 = True)
        for base, dirs, files in os.walk(self.exportFolder):
            if 'export.zip' in files:
                files.remove('export.zip')
            for file in files:
                fn = os.path.join(base, file)
                zfile.write(fn, os.path.join(os.path.basename(base),file))
        zfile.close()

    # Insert data file by file and line by line.
    def insertTable(self):
        os.makedirs(self.importFolder)

        # Unzipping the given ZIP files.
        self.unzipFile()

        self.importSumLog = open(self.importLogfile,"w")
        self.importSumLog.write('Starting import!\n\n')
        self.importSumLog.close()

        self.tableDontExist = []

        # Get all files
        for self.filename in os.listdir(self.importDbDumpFiles):
            if self.filename.endswith(".txt"):
                self.table_name = self.filename.replace(".txt", "")

                try:
                    self.table = self.meta.tables[self.table_name]
                    self.fullFilename = os.path.join(self.importDbDumpFiles, self.filename)
                    self.totalLines = sum(1 for line in open(self.fullFilename))

                    # Disabling trigger to off the contrainst
                    if self.databaseType == 'postgresql':
                        self.disableTriggerSql = 'ALTER TABLE "%s" DISABLE TRIGGER ALL;' %self.table_name   
                    if self.databaseType == 'mysql+mysqlconnector':
                        self.disableTriggerSql = 'SET FOREIGN_KEY_CHECKS=0;'
                    self.conn.execute(self.disableTriggerSql)

                    # Delete rows
                    self.deleteStarTableSql = self.table.delete()
                    self.conn.execute(self.deleteStarTableSql)

                    if self.totalLines == 0:
                        self.importSumLog = open(self.importLogfile,"a")
                        self.importSumLog.write('Importing data into %s. [0/0]\n' %self.table_name)
                        self.importSumLog.close()
                    else:
                        self.importSumLog = open(self.importLogfile,"a")
                        self.importSumLog.write('\n')
                        self.importSumLog.close()
                    
                    # Inserting data line by line
                    with open(self.fullFilename, 'r') as file:
                        i = 1
                        for self.data in file:
                            with open(self.importLogfile, "r") as readTempImportFile:
                                lines = readTempImportFile.readlines()
                                lines[-1] = 'Importing data into %s. [%s/%s]\n' %(self.table_name, i, self.totalLines)
                            readTempImportFile.close()

                            with open(self.importLogfile, "w") as writeTempImportFile:
                                for line in lines:
                                    writeTempImportFile.write(line)
                            writeTempImportFile.close()

                            self.data = eval(self.data)           
                            self.insertRowSql = self.table.insert(self.data)
                            self.conn.execute(self.insertRowSql)
                            i += 1

                    # Enabling triggers back
                    if self.databaseType == 'postgresql':
                        self.enableTriggerSql = 'ALTER TABLE "%s" ENABLE TRIGGER ALL;' %self.table_name
                    if self.databaseType == 'mysql+mysqlconnector':
                        self.enableTriggerSql = 'SET FOREIGN_KEY_CHECKS=1;'
                    self.conn.execute(self.enableTriggerSql)

                    if self.databaseType == 'postgresql':
                        self.rowCountSql = 'SELECT COUNT(*) FROM ' + '"%s"' %str(self.table_name)
                    if self.databaseType == 'mysql+mysqlconnector':
                        self.rowCountSql = 'SELECT COUNT(*) FROM ' + '`%s`.' %str(self.databaseName) + '%s' %str(self.table_name)
                    self.rowCount = self.engine.execute(self.rowCountSql)
                    self.rowCountResult = self.rowCount.fetchall()
                    self.rowCountResult = self.rowCountResult[0][0]
                    self.allTablesFile= open(self.importDbTableStatsLogfile,"a")
                    self.allTablesFile.write("%s:%s\n" % (self.table_name, self.rowCountResult))
                    self.allTablesFile.close()
                    pass
                except KeyError:
                    self.importSumLog = open(self.importLogfile,"a")
                    self.importSumLog.write('Table %s does not exist\n' %self.table_name)
                    self.importSumLog.close()
                    self.tableDontExist.append(self.table_name)

        self.importSumLog = open(self.importLogfile,"a")
        self.importSumLog.write('\nImport is complete!\n\n')
        self.importSumLog.close()

        if self.tableDontExist:
            self.importSumLog = open(self.importLogfile,"a")
            self.importSumLog.write('The following table(s) are not imported because they do not exist in the target database:\n')
            for names in self.tableDontExist:
                self.importSumLog.write('%s\n' %names)
            self.importSumLog.close()

        self.insertSummary()

    # Compare export_db_table_stats.log with import_db_table_stats.log
    def insertSummary(self):
        # Read the export_db_table_stats.log and store in as a dictonary.
        exportStats = {}
        self.exportStatsLogfile = os.path.join(self.basePathFolder, 'stats', 'export_db_table_stats.log')
        with open(self.exportStatsLogfile, 'r') as n:
            for line in n:
                (key, val) = line.strip().split(':')
                exportStats[key] = val
        n.close

        # Read the import_db_table_stats.log and store in as a dictonary.
        importStats = {}
        with open(self.importDbTableStatsLogfile, 'r') as m:
            for line in m:
                (key, val) = line.strip().split(':')
                importStats[key] = val
        m.close

        diffkeys = [k for k in exportStats if exportStats[k] != importStats[k]]
        self.importSumLog = open(self.importLogfile,"a")
        self.importSumLog.write('\n"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""\n')
        self.importSumLog.write('Import and Export Mismatch Summary\n\n')
        self.importSumLog.write('table : exported lines - imported lines\n')
        for k in diffkeys:
            self.importSumLog.write(k + ':' + exportStats[k] + ' - ' + importStats[k] + '\n')
        self.importSumLog.write('\n"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""\n')
        self.importSumLog.write("\nNOTE: If you don't see any mismatch is being reported here, then the exported lines are the same as imported lines!\n") 
        self.importSumLog.close()

    # UnZIP the export contents.
    def unzipFile(self):
        zip_ref = zipfile.ZipFile(self.basePath, 'r')
        zip_ref.extractall(self.basePathFolder)
        zip_ref.close()
